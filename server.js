/*server.js*/

const http = require('http');
const fs = require('fs');

const hostname = 'ec2-18-212-1-162.compute-1.amazonaws.com';

const port = 3000;

http.createServer(function(request, response) {
  response.writeHead(200, {'Content-Type': 'text/html'});

  var file = fs.createReadStream('index.html');
  file.pipe(response);

}).listen(port);

console.log('Server running at http://' + 'hostname' + ':' + port + '/');